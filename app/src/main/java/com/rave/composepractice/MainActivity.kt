package com.rave.composepractice

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.rave.composepractice.ui.theme.ComposePracticeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposePracticeTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {

                }
            }
        }
    }
}

//@Preview
//@Composable
//fun GreetingCardPreview(){
//    ComposePracticeTheme {
//    }
//}

//@Composable
//fun Greeting(name: String) {
//    Text(text = "Hello $name!")
////    Text(text = "How are you $name?")
////    GreetingCard()
//}

//@Preview(showBackground = true)
//@Composable
//fun DefaultPreview() {
//    ComposePracticeTheme {
//        Column {
//            Greeting("Calvin")
//        }
//    }
//}