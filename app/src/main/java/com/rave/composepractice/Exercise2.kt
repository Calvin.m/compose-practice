package com.rave.composepractice

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.rave.composepractice.ui.theme.ComposePracticeTheme
import androidx.compose.ui.unit.sp
//import androidx.compose.ui.graphics.Color.Green
//import androidx.compose.ui.graphics.Color.Yellow
//import androidx.compose.ui.graphics.Color.Cyan
//import androidx.compose.ui.graphics.Color.LightGray

@Composable
fun BirthdayGreetingWithImage(first: String, second: String, third: String) {
    val image = painterResource(R.drawable.bg_compose_background)
    Column{
        Image(
            painter = image,
            contentDescription = null
        )
        Column(modifier = Modifier.padding(16.dp)) {
            Text(
                text = first,
                fontSize = 24.sp,
                textAlign = TextAlign.Justify,
            )
        }
        Column(modifier = Modifier.padding(start = 16.dp, end = 16.dp)) {
            Text(
                text = second,
                textAlign = TextAlign.Justify
            )
        }
        Column(modifier = Modifier.padding(16.dp)) {
            Text(
                text = third,
                textAlign = TextAlign.Justify
            )
        }
    }
}

@Preview(showBackground = false)
@Composable
fun Exercise2ComposeArticle() {
    ComposePracticeTheme() {
        BirthdayGreetingWithImage(
            "Jetpack Compose tutorial",
            "Jetpack Compose is a modern toolkit for building native Android UI. Compose simplifies and accelerates UI development on Android with less code, powerful tools, and intuitive Kotlin APIs.",
            "In this tutorial, you build a simple UI component with declarative functions. You call Compose functions to say what elements you want and the Compose compiler does the rest. Compose is built around Composable functions. These functions let you define your app\\'s UI programmatically because they let you describe how it should look and provide data dependencies, rather than focus on the process of the UI\\'s construction, such as initializing an element and then attaching it to a parent. To create a Composable function, you add the @Composable annotation to the function name.\n"
        )
    }
}

@Composable
fun TaskManager() {
    val image = painterResource(R.drawable.ic_task_completed)
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(painter = image, contentDescription = null)
        Text("All tasks completed\nNice work!",
            textAlign = TextAlign.Center,)
    }
}

@Preview(showBackground = false)
@Composable
fun Exercise2TaskManager() {
    ComposePracticeTheme {
        TaskManager()
    }
}

@Composable
fun ComposeQuadrant() {
    Column() {
        Row(Modifier.weight(1f)) {
            TextBlock(
                backgroundColor = Color.Green,
                modifier = Modifier.weight(1f),
                title = "Text composable",
                body = "Displays text and follows Material Design guidelines."
            )
            TextBlock(
                backgroundColor = Color.Yellow,
                modifier = Modifier.weight(1f),
                title = "Image composable",
                body = "Creates a composable that lays out and draws a given Painter class object."
            )
        }
        Row(Modifier.weight(1f)) {
            TextBlock(
                backgroundColor = Color.Cyan,
                modifier = Modifier.weight(1f),
                title = "Row composable",
                body = "A layout composable that places its children in a horizontal sequence."
            )
            TextBlock(
                backgroundColor = Color.LightGray,
                modifier = Modifier.weight(1f),
                title = "Column composable",
                body = "A layout composable that places its children in a vertical sequence."
            )
        }
    }
}

@Composable
private fun TextBlock(
    title: String,
    body: String,
    backgroundColor: Color,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .background(backgroundColor)
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(text = title, textAlign = TextAlign.Justify, fontWeight = FontWeight.Bold)
        Text(text = body, textAlign = TextAlign.Justify)
    }
}

@Preview(showBackground = false)
@Composable
fun Exercise3ComposeQuadrant() {
    ComposePracticeTheme() {
        ComposeQuadrant()
    }
}
