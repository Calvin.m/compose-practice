package com.rave.composepractice

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.rave.composepractice.ui.theme.ComposePracticeTheme
import androidx.compose.ui.unit.sp


@Composable
fun BusinessCard() {
    val image = painterResource(R.drawable.android_head)
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(painter = image, contentDescription = null)
        Text(
            text = "Calvin Macintosh",
            fontSize = 36.sp,
        )
        Text(
            text = "Android Developer",
            fontSize = 24.sp,
        )
        Row(Modifier.padding(top = 120.dp)) {
            Text("(123)-456-7890")
        }
        Row() {
            Text("@socialMediaHandle")
        }
        Row() {
            Text("calvin.m@ravebizz.com")
        }

    }
}

@Preview(showBackground = false)
@Composable
fun Exercise3Preview() {
    ComposePracticeTheme {
        BusinessCard()
    }
}
